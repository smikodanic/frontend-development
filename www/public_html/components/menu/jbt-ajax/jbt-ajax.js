/**
 * jQuery code for JBT - jQuery Bootstrap Tree
 *
 * Functionality:
 * 		- toogle collapsing elements
 *   	- setting up active class
 *    	- cookie memorisation of the link that was last clicked
 */

$(document).ready(function(){

	////////////// AJAX //////////////////////

	//initial ajax load
	$('.jbt-menu').load('./ajax-data/00.html');

	//define ajax function
	var ajaxLoad=function(url_val, that){
		
		var onAjaxSuccess=function(respons){
 			$(that).after(respons);
 			$(that).nextUntil('.list-group-item.header1, .list-group-item.header2').not('.header1, .header2').hide().fadeIn();
 			// console.log(respons);
		}

		var onAjaxError=function(){
 			$(that).after('<li><b style="color:red;font:10px Verdana;">ERROR! File '+url_val+' doesnt exist!</b></li>');
		}

		// //ajax load file into variable
		$.ajax({
    		url: url_val,
    		dataType:'html',
    		crossDomain:false,
    		success: onAjaxSuccess,
    		error: onAjaxError });
	}

	//ajax loader for li tags which dont have a tag as a child
	$('body').on('click', '.jbt-menu .list-group-item:not(:has(a))', function(){
		
		//get clicked id value
		var clicked_id=$(this).attr('id');

		var that = this;
		var url_val='./ajax-data/'+clicked_id+'.html';

		//check if the li item is already clicked
		var tf=$(this).hasClass('clicked');

		if(!tf) {
			$(this).addClass('clicked'); //add .clicked to UL tag
			ajaxLoad(url_val, that);
		}
		else {
			// $('*').removeClass('clicked');
			$(this).nextUntil('.list-group-item.header1, .list-group-item.header2').not('.header1, .header2').toggle('900');
		}

		// console.log(this);

	});



	/////////// COOKIE //////////////////
	//get text from clicked link and set cookie
	$('body').on('click', '.jbt-menu  .list-group-item > a', function() {
		var a_txt=$(this).text();
		
		//delete all posiblle cookies if exists
		$.removeCookie("a_txt");

		//set cookie that expires in 10 days
		$.cookie('a_txt', a_txt, {expires:10});
		
		//console.log(a_txt);
	});

	//get cookie
	var a_txt_cookie=$.cookie('a_txt');
	a_txt_cookie=decodeURIComponent(a_txt_cookie); //replace %20 to empty character in a string

	//set active link from cookie
	if(a_txt_cookie != null) {
		
		$('.jbt-menu .active').removeClass('active');

		$('.jbt-menu .list-group-item > a:contains('+a_txt_cookie+')')
			.filter(function(){
				return $(this).text() === a_txt_cookie;
			})
			.parentsUntil()
			.addClass('active');
	}



	/////////// COLLAPSING FEATURES /////////////

	//init - collapse tree menu on first load
	$('.jbt-menu .list-group-item.header1').nextUntil('.list-group-item.header1').css('display','none');
	$('.list-group-item.header2').nextUntil('.list-group-item.header2').css('display','none');

	//set active class on clicked link
	$('body').on('click', '.jbt-menu  .list-group-item > a', function() {
		$('.active').removeClass('active');
		$(this).parentsUntil().addClass('active');
	});

	//expand all
	$('body').on('click','.jbt-menu #jbt1', function(){
		$('.list-group-item.header1').nextUntil('.list-group-item.header1').css('display','block');
		$('.list-group-item.header2').nextUntil('.list-group-item.header2').css('display','block');
	});

	//collapse all
	$('body').on('click', '.jbt-menu  #jbt2',  function(){
		$('.list-group-item.header1').nextUntil('.list-group-item.header1').css('display','none');
		$('.list-group-item.header2').nextUntil('.list-group-item.header2').css('display','none');
	});

	//show active
	$('body').on('click', '.jbt-menu  #jbt3', function(){
		// $('.active')
		// .parentsUntil()
		// .children('li')
		// .css('display','block');
		$('li.active').css('display','block');
	});
	
	
	//console.log(a_txt_cookie);

});