/**
 * jQuery code for JBT - jQuery Bootstrap Tree
 *
 * Functionality:
 * 		- toogle collapsing elements
 *   	- setting up active class
 *    	- cookie memorisation of the link that was last clicked
 */

$(document).ready(function(){

	//init - collapse tree menu
	$('.jbt-menu .list-group-item.header1').nextUntil('.list-group-item.header1').css('display','none');
	$('.list-group-item.header2').nextUntil('.list-group-item.header2').css('display','none');

	$('.jbt-menu .list-group-item.header1, .list-group-item.header2').click(function(){
		$(this).nextUntil('.list-group-item.header1, .list-group-item.header2').not('.header1, .header2').toggle('900');
	});

	//set active class on clicked link
	$('.jbt-menu  .list-group-item > a').click(function() {
		$('.active').removeClass('active');
		$(this).parentsUntil().addClass('active');
	});

	//get text from clicked link and set cookie
	$('.jbt-menu  .list-group-item > a').click(function() {
		var a_txt=$(this).text();
		
		//delete all posiblle cookies if exists
		$.removeCookie("a_txt");

		//set cookie that expires in 10 days
		$.cookie('a_txt', a_txt, {expires:10});
		
		//console.log(a_txt);
	});

	//get cookie
	var a_txt_cookie=$.cookie('a_txt');
	a_txt_cookie=decodeURIComponent(a_txt_cookie); //replace %20 to empty character in a string

	//set active link from cookie
	if(a_txt_cookie != null) {
		
		$('.jbt-menu .active').removeClass('active');

		$('.jbt-menu .list-group-item > a:contains('+a_txt_cookie+')')
			.filter(function(){
				return $(this).text() === a_txt_cookie;
			})
			.parentsUntil()
			.addClass('active');
	}

	//expand all
	$('.jbt-menu #jbt1').click(function(){
		$('.list-group-item.header1').nextUntil('.list-group-item.header1').css('display','block');
		$('.list-group-item.header2').nextUntil('.list-group-item.header2').css('display','block');
	});

	//collapse all
	$('.jbt-menu  #jbt2').click(function(){
		$('.list-group-item.header1').nextUntil('.list-group-item.header1').css('display','none');
		$('.list-group-item.header2').nextUntil('.list-group-item.header2').css('display','none');
	});

	//show active
	$('.jbt-menu  #jbt3').click(function(){
		// $('.active')
		// .parentsUntil()
		// .children('li')
		// .css('display','block');
		$('li.active').css('display','block');
	});
	
	
	//console.log(a_txt_cookie);

});