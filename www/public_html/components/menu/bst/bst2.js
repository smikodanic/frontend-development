/**
 * jQuery code for BST2 - Bootstrap Scrolling Tabs 2
 *
 * Functionality:
 *    - tab scrolling
 *    - toggle tab content
 */

$(document).ready(function(){

	  /**
	   * TAB SCROLLING
	   */

	  //number of li elements inside .nav-tabs
	  //e.g. number of tabs
	  var li_num=$('.bst-menu .nav-tabs>li').length;

	  //*** sum of all li (tab) widths ***//
	  var li_wd_sum=0;
	  for(var i=0; i<li_num; i++) {

	  	//get the length of li element
	  	var li_wd=$('.bst-menu .nav-tabs>li').eq(i).width();

	  	//total sum
	  	li_wd_sum+=li_wd;

	  	//console.log(li_wd+' - '+li_wd_sum);
	  }

	  //define nav_tabs minimal width
	  var ntmw=li_wd_sum+'px';
	  $('.bst-menu .nav-tabs').css('min-width', ntmw);

	  //showing scrollbar on mouseover
	  $('.bst-menu .tab-container').hover(

	  	function() { 
	  		$(this).css('overflow-x','scroll');
	  	},
	  	function() {
	  		$(this).css('overflow-x','hidden');
	  	}

	  );



	  /**
	   * REMOVE TAB FOCUS after click 
	   * (removing dashed border after click)
	   */
	  	$('.bst-menu .nav-tabs>li>a').click(function(){
	  		$(this).blur();
	  	}); 
	  	


	  	/**
	  	 *  TOGGLE TAB CONTENT
	  	 * - binding 'click' event handler to dynamically created '.active' css class with .on('click') method
	  	 * - method .click() will not work
	  	 */
	  	$('body').on('click', '.bst-menu .nav-tabs>li.active>a', function(){
	  		
	  		//get href value of active tab
	  		var href_val=$(this).attr('href'); //gives for example '#b'

	  		
	  		var sel = '.bst-menu .tab-content .tab-pane'+href_val;
	  		var tf=$(sel).hasClass('active'); // return true or false

	  		//colapse (hide) tab content by removing 'active' CSS class
	  		if(tf) $(sel).removeClass('active');

	  		//showing tab content by adding 'active' class
	  		else $(sel).addClass('active');

	  	});

});