/**
 * jQuery code for BST - Bootstrap Scrolling Tabs
 *
 * Functionality:
 *    - tab shifting with left and right arrow
 *    - toggle tab content
 */

$(document).ready(function(){

	  /**
	   * Tab shifting
	   */

	  // tab-container width
	  var tc_wd=$('.bst-menu .tab-container').width();

	  //number of li elements inside .nav-tabs
	  //e.g. number of tabs
	  var li_num=$('.bst-menu .nav-tabs>li').length;

	  //*** sum of all li (tab) widths ***//
	  var li_wd_sum=0;
	  for(var i=0; i<li_num; i++) {

	  	//get the length of li element
	  	var li_wd=$('.bst-menu .nav-tabs>li').eq(i).width();

	  	//sum
	  	li_wd_sum=li_wd_sum+li_wd;

	  	//console.log(li_wd+' - '+li_wd_sum);
	  }

	  //*** define shift value ***//
	  if(tc_wd >= li_wd_sum) var shift=0;
	  else var shift=tc_wd;


	  //get left position in position:relative CSS
	  var posLeft = function(){
  			return $('.nav-tabs').position().left;
		};

	  //get initial position
	  var posInit=posLeft();


		//disabling arrows under certain conditions
		var disableArrows = function( posleft ){
			//disabling right arrow
		  	if(posleft <= posInit + tc_wd - li_wd_sum ) $('.arrow.arrow-right').attr('disabled', 'disabled');
		  	else $('.arrow.arrow-right').attr('disabled', false);

		  	//disabling left arrow
		  	if(posleft >= posInit ) $('.arrow.arrow-left').attr('disabled', 'disabled');
		  	else $('.arrow.arrow-left').attr('disabled', false);
		}


	  	//SHIFT RIGHT
	  	$('.arrow.arrow-right').click(function(){
	  		
	  		//disable arrow unless animation is finished
	  		$('.arrow.arrow-right').attr('disabled', 'disabled');

	  		$('.nav-tabs').css('position','relative').animate({'left': (posLeft() - shift - posInit)+'px'}, 1000, function() {
	  			
				//enable arrow after animation is finished
				$('.arrow.arrow-right').attr('disabled', false);

				//disable arrow if limit is reached
				disableArrows( posLeft() );

	  			// console.log( posLeft() );
	  		});
	  	
	  	});
	

	  	//SHIFT LEFT
	  	$('.arrow.arrow-left').click(function(){

	  		//disable arrow unless animation is finished
	  		$('.arrow.arrow-left').attr('disabled', 'disabled');
	  		
	  		$('.nav-tabs').css('position','relative').animate({'left': (posLeft() + shift - posInit)+'px'}, 1000, function() {
	  			
				//enable arrow after animation is finished
				$('.arrow.arrow-right').attr('disabled', false);

				//disable arrow if limit is reached
				disableArrows( posLeft() );

	
	  			// console.log( posLeft() );
	  		});

	  	});


	  /**
	   * REMOVE TAB FOCUS after click
	   * (removing dashed border after click)
	   */
	  	$('.bst-menu .nav-tabs>li>a').click(function(){
	  		$(this).blur();
	  	}); 
	  	


	  	/**
	  	 * TOGGLE TAB CONTENT
	  	 * - binding 'click' event handler to dynamically created '.active' css class with .on('click') method
	  	 * - method .click() will not work
	  	 */
	  	$('body').on('click', '.bst-menu .nav-tabs>li.active>a', function(){
	  		
	  		//get href value of active tab
	  		var href_val=$(this).attr('href'); //gives for example '#b'

	  		
	  		var sel = '.bst-menu .tab-content .tab-pane'+href_val;
	  		var tf=$(sel).hasClass('active'); // return true or false

	  		//colapse (hide) tab content by removing 'active' CSS class
	  		if(tf) $(sel).removeClass('active');

	  		//showing tab content by adding 'active' class
	  		else $(sel).addClass('active');

	  	});

	
	  //console.log(posInit + tc_wd - li_wd_sum);

});